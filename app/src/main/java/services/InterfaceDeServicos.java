package services;

import java.util.List;

import dto.LoginDTO;
import dto.UserDTO;
import dto.UserResponseDTO;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface InterfaceDeServicos {

    @GET("/users")
    Call<List<UserDTO>> usuarios();

    @POST("/users")
    Call<UserDTO> cadastrar(@Body UserDTO user);

    @POST("/auth/login")
    Call<LoginDTO> login(@Body LoginDTO loginDto);

    @GET("/users")
    Call<UserResponseDTO> getUsers(@Header("Authorization") String authorization);

    @PUT("/users/{id}")
    Call<UserDTO> alterarUsuario(@Body UserDTO userDTO, @Path("id") int id, @Header("Authorization") String authorization);

    @DELETE("/users/{id}")
    Call<Void> excluirUsuario(@Path("id") int id, @Header("Authorization") String authorization);
}