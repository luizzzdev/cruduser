package com.example.cruduser.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.cruduser.R;

import dto.UserDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.RetrofitService;

public class AlterarUsuarioActivity extends AppCompatActivity {

    EditText et_email;
    EditText et_nome;
    EditText et_telefone;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);

        Intent intent = getIntent();
        id = intent.getIntExtra("id", -1);
        String nome = intent.getStringExtra("nome");
        String email = intent.getStringExtra("email");
        String phone = intent.getStringExtra("tel");

        et_email = findViewById(R.id.et_cadastro_usuario_email);
        et_nome = findViewById(R.id.et_cadastro_usuario_nome);
        et_telefone = findViewById(R.id.et_cadastro_usuario_telefone);

        et_email.setText(email);
        et_nome.setText(nome);
        et_telefone.setText(phone);
    }

    public void cadastrar(View view) {
        String nome = ((EditText) findViewById(R.id.et_cadastro_usuario_nome)).getText().toString();
        String email = ((EditText) findViewById(R.id.et_cadastro_usuario_email)).getText().toString();
        String senha = ((EditText) findViewById(R.id.et_cadastro_usuario_nome)).getText().toString();
        String telefone = ((EditText) findViewById(R.id.et_cadastro_usuario_telefone)).getText().toString();

        UserDTO userDTO = new UserDTO(nome, email, senha.isEmpty() ? null : senha, telefone);

        RetrofitService.getServico(this).alterarUsuario(userDTO, id, this.getToken()).enqueue(new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                UserDTO userDto = response.body();
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {

            }
        });
    }

    private String getToken() {
        SharedPreferences sharedPreferences = getSharedPreferences("storage", 0);
        String token = sharedPreferences.getString("token", null);
        return token == null ? null : "Bearer " + token;
    }
}
