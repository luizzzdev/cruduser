package com.example.cruduser.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.cruduser.R;

import java.util.ArrayList;
import java.util.List;

import dto.UserDTO;
import dto.UserResponseDTO;
import helpers.SwipeToDeleteCallback;
import helpers.UsuarioAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.RetrofitService;

public class ListaUsuariosActivity extends AppCompatActivity {

    private static final String TAG = "ListaUsuariosActivity";
    private List<UserDTO> usuarios = new ArrayList<>();

    public void exclurItem(int id) {
        String token = getToken();

        RetrofitService.getServico(this).excluirUsuario(id, token).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Toast.makeText("Codigo de retorno: " + response.code(), Toast.LENGTH_SHORT);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_usuarios);
        initUserList();
    }

    private void initUserList() {
        SharedPreferences sharedPreferences = getSharedPreferences("storage", 0);
        String token = sharedPreferences.getString("token", null);

        if (token == null) {
            startActivity(new Intent(ListaUsuariosActivity.this, LoginActivity.class));
        }

        RetrofitService.getServico(this).getUsers("Bearer " + token).enqueue(new Callback<UserResponseDTO>() {
            @Override
            public void onResponse(Call<UserResponseDTO> call, Response<UserResponseDTO> response) {
                Log.d(TAG, "onResponse: list users");

                for(UserDTO userDTO: response.body().getContent()) {
                    usuarios.add(userDTO);
                }

                initRecyclerView();
            }

            @Override
            public void onFailure(Call<UserResponseDTO> call, Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.rv_todos_usuarios);
        UsuarioAdapter usuarioAdapter = new UsuarioAdapter(this, usuarios);
        recyclerView.setAdapter(usuarioAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteCallback(usuarioAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

}
