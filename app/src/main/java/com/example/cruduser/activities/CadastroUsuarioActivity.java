package com.example.cruduser.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cruduser.R;

import dto.UserDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.RetrofitService;

public class CadastroUsuarioActivity extends AppCompatActivity {
    private static final String TAG = "CadastroUsuarioActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);
    }

    public void cadastrar(View view) {
        String nome = ((EditText) findViewById(R.id.et_cadastro_usuario_nome)).getText().toString();
        String telefone = ((EditText) findViewById(R.id.et_cadastro_usuario_telefone)).getText().toString();
        String email = ((EditText) findViewById(R.id.et_cadastro_usuario_email)).getText().toString();
        String senha = ((EditText) findViewById(R.id.et_cadastro_usuario_password)).getText().toString();

        UserDTO user = new UserDTO(nome, email, senha, telefone);

        RetrofitService.getServico(this).cadastrar(user).enqueue(new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                Log.v(TAG, "Deu certo a requisição!");
                Toast.makeText(CadastroUsuarioActivity.this, "Novo usuario com ID : " + response.body().getId(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.v(TAG, t.getMessage());
            }
        });
    }
}
