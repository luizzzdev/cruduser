package com.example.cruduser.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.cruduser.R;

import dto.LoginDTO;
import dto.UserDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import services.RetrofitService;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void login(View view) {
        String email = ((EditText) findViewById(R.id.et_login_email)).getText().toString();
        String password = ((EditText) findViewById(R.id.et_login_password)).getText().toString();

        LoginDTO userLoginDTO = new LoginDTO(email, password, null);


        RetrofitService.getServico(this).login(userLoginDTO).enqueue(new Callback<LoginDTO>() {
            @Override
            public void onResponse(Call<LoginDTO> call, Response<LoginDTO> response) {
                Toast.makeText(LoginActivity.this, "Usuario logado", Toast.LENGTH_SHORT);

                SharedPreferences sharedPreferences = getSharedPreferences("storage", 0);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("email", response.body().getEmail());
                editor.putString("token", response.body().getToken());
                editor.apply();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }

            @Override
            public void onFailure(Call<LoginDTO> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}
