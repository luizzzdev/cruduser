package dto;

import java.util.List;

public class UserResponseDTO {
    private List<UserDTO> content;

    public List<UserDTO> getContent() {
        return content;
    }
}
