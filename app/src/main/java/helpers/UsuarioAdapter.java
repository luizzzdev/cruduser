package helpers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cruduser.R;
import com.example.cruduser.activities.AlterarUsuarioActivity;
import com.example.cruduser.activities.ListaUsuariosActivity;

import java.util.List;

import dto.UserDTO;

public class UsuarioAdapter extends RecyclerView.Adapter<UsuarioAdapter.UsuarioHolder> {
    private LayoutInflater mInflater;
    private Context context;
    private List<UserDTO> listaUsuarios;


    public UsuarioAdapter(Context context, List<UserDTO> listaUsuarios) {
        this.context = context;
        this.listaUsuarios = listaUsuarios;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public UsuarioHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recyclerview_layout_item_todos_usuarios, viewGroup, false);
        UsuarioHolder holder = new UsuarioHolder(view);
        return holder;
    }

    public void deleteItem(int position) {
        mRecentlyDeletedItem = list.get(position);
        mRecentlyDeletedItemPoisition = position;
        lista.remove(position);
        notifyRemoved(position);
        showAlertDialogButtonClicked();
    }

    public void undoDelete() {
        lista.add(mRecentlyDeletedItemPoisition, mRecentlyDeletedItem);
        notifyItemInserted(mRecentlyDeletedItemPoisition);
    }

    public void showAlertDialogButtonClicked() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Exclusao de usuario");
        builder.setMessage("Voce confirma a acao?");

        builder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                excluir();
            }
        });

        builder.setNegativeButton("Nao", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                undoDelete();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onBindViewHolder(@NonNull UsuarioHolder usuarioHolder, int position) {
        String nome = listaUsuarios.get(position).getName();
        usuarioHolder.nome.setText(nome);
    }

    @Override
    public int getItemCount() {
        return listaUsuarios.size();
    }

    public class UsuarioHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final TextView nome;

        public UsuarioHolder(@NonNull View itemView) {
            super(itemView);
            nome = itemView.findViewById(R.id.tv_recyclerview_nome_usuario);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            UserDTO user = listaUsuarios.get(getLayoutPosition());
            Intent alterarUsuarioIntent = new Intent(context, AlterarUsuarioActivity.class);
            alterarUsuarioIntent.putExtra("id", user.getId());
            alterarUsuarioIntent.putExtra("nome", user.getName());
            alterarUsuarioIntent.putExtra("email", user.getEmail());
            alterarUsuarioIntent.putExtra("tel", user.getPhone());
            context.startActivity(alterarUsuarioIntent);
        }
    }
}
